% Introduction
\chapter{Introduction}

\section{Why has Libre-SOC chosen PowerPC ?}

For a hybrid CPU-VPU-GPU, intended for mass-volume adoption in tablets,
netbooks, chromebooks and industrial embedded (SBC) systems, our choice was
between Nyuzi, MIAOW, RISC-V, PowerPC, MIPS and OpenRISC.

Of all the options, the PowerPC architecture is more complete and far more
mature. It also has a deeper adoption by Linux distributions.

Following IBM's release of the Power Architecture instruction set to the Linux
Foundation in August 2019 the barrier to using it is no more than that of using
RISC-V. We are encouraged that the OpenPOWER Foundation is supportive of what
we are doing and helping, e.g by putting us in touch with people who can help
us.

\subsection{Summary}

\vspace{-0.1in}
\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item
    We propose the standardisation of the way that the \gls{PowerPC} Instruction Set
    Architecture (PPC ISA) is extended, enabling many different flavours within a
    well supported family to co-exist, long-term, without conflict, right across
    the board.

\item

    This is about more than just our project. Our proposals will facilitate the
    use of PPC in novel or niche applications without breaking the PPC ISA into
    incompatible islands.

\item

    PPC will gain a competitive market advantage by removing the need for
    separate VPU or GPU functions in RTL or ASICs thus enabling lower cost
    systems. Libre-SOC's project is to extend the PPC to integrate the GPU and
    VPU functionality directly as part of the PPC ISA (example: Broadcom
    VideoCore IV being based around extensions to an ARC core).

\item

    Libre-SOC's extensions will be easily adopted, as the standard GNU/Linux
    distributions will very deliberately run unmodified on our ISA, including
    full compatibility with illegal instruction trap requirements.

\end{itemize}

\subsection{One CPU multiple ISAs}

This is a quick overview of the way that we would like to add changes that we
are proposing to the PowerPC instruction set (ISA). It is based on a Open
Standardisation of the way that existing \textbf{mode switches}, already found in the
POWER instruction set, are added:

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

    FPSCR's \textbf{NI} bit, setting non-\gls{IEEE754} FP mode

\item

    MSR's \textbf{LE} bit (and associated HILE bit), setting little-endian mode

\item

    MSR's \textbf{SF} bit, setting either 32-bit or 64-bit mode

\item

    PCR's \textbf{compatibility} bits 60-62, V2.05 V2.06 V2.07 mode

\end{itemize}

[It is well-noted that unless each \textbf{mode switch} bit is set, any alternative
(additional) instructions (and functionality) are completely inaccessible, and
will result in \textbf{illegal instruction} traps being thrown. This is recognised as
being critically important.]

These bits effectively create multiple, incompatible run-time switchable ISAs
within one CPU. They are selectable for the needs of the individual program (or
OS) being run.

All of these bits are set by an instruction, that, once set, radically changes
the entire behaviour and characteristics of subsequent instructions.

With these (and other) long-established precedents already in POWER, there is
therefore essentially conceptually nothing new about what we propose: we simply
seek that the process by which such \textbf{switching} is added is formalised and
standardised, such that we (and others, including IBM itself) have a clear,
well-defined standards-non-disruptive, atomic and non-intrusive path to extend
the POWER ISA for use in markets that it presently cannot enter.

We advocate that some of \textbf{mode-setting} (escape-sequencing) bits be binary
encoded, some unary encoded, and that some space marked for \textbf{offical} use, some
\textbf{experimental}, some \textbf{custom} and some \textbf{reserved}. The available space in a
suitably-chosen SPR to be formalised, and recommend the OpenPOWER Foundation be
given the IANA-like role in atomically allocating mode bits.

The IANA-like atomic role ensures that new PCR mode bits are allocated
world-wide unique. In combination with a mandatory illegal instruction
exception to be thrown on any system not supporting any given mode, the
opportunity exists for all systems to trap and emulate all other systems and
thus retain some semblance of interoperability. (Contrast this with either
allocating the same mode bit(s) to two (or more) designers, or not making
illegal exceptions mandatory: binary interoperability becomes unachievable and
the result is irrevocable damage to POWER's reputation.)

We also advocate to consider reserving some bits as a \textbf{countdown} where the new
mode will be enabled only for a certain number of instructions. This avoids an
explicit need to \textbf{flip back}, reducing binary code size. Note that it is not a
good idea to let the counter cross a branch or other change in PC (and to throw
illegal instruction trap if attempted). However traps and exceptions themselves
will need to save (and restore) the countdown, just as the rest of the PCR and
other modeswitching bits need to be saved.

Instructions that we need to add, which are a normal part of GPUs, include
ATAN2, LOG, NORMALISE, YUV2RGB, Khronos Compliance FP mode (different from both
IEEE754 and \textbf{NI} mode), and many more. Many of these may turn out to be useful
in a wider context: they however need to be fully isolated behind
\textbf{mode-setting} before being in any way considered for Standards-track formal
adoption.

Some mode-setting instructions are privileged, i.e can only be set by the
kernel (e.g 32 or 64 bit mode). Most of the escape sequences that we propose
will be (have to be) usable without the need for an expensive system call
overhead (because some of the instructions needed will be in extremely tight
inner loops).

\subsection{About Libre-SOC Commercial Project}

The Libre-SOC Commercial Product is a hybrid \gls{CPU}-\gls{GPU}-\gls{VPU} intended for
mass-volume production. There is no separate GPU, because the CPU is the GPU.
There is no separate VPU, because the CPU is the GPU. There is not even a
separate pipeline: the CPU pipelines are the GPU and VPU pipelines.

Closest equivalents include the ARC core (which has VPU extensions and 3D
extensions in the form of Broadcom's \gls{VideoCoreIV}) and the \gls{ICubeCorpIC3128}.
Both are considered \textbf{hybrid} CPU-GPU-VPU processors.

\textbf{Normal} Commercial GPUs are entirely separate processors. The development cost
and complexity purely in terms of Software Drivers alone is immense. We reject
that approach (and as a small team we do not have the resources anyway).

With the project being Libre - not proprietary and secretive and never to be
published, ever - it is no good having the extensions as \textbf{custom} because
\textbf{custom} is specifically for the cases where the augmented toolchain is never,
under any circumstances, published and made public by the proprietary company
(and would never be accepted upstream anyway). For business commercial reasons,
Libre-SOC is the total opposite of this proprietary, secretive approach.

Therefore, to meet our business objectives:

\begin{itemize}
\parskip 0pt
\itemsep 1pt

\item

    As shown from Nyuzi and Larrabee, although ideally suited to high
    performance compute tasks, a \textbf{traditional} general-purpose full
    IEEE754-compliant Vector ISA (such as that in POWER9) is not an adequate
    basis for a commercially competitive GPU. Nyuzi's conclusion is that using
    such general-purpose Vector ISAs results in reaching only 25% performance
    (or requiring 4-fold increase in power consumption) to achieve par with
    current commercial-grade GPUs.

\item

    We are not going the \textbf{traditional} (separate custom GPU) route because it
    is not practical for a new team to design hardware and spend 8+ man-years
    on massively complex inter-processor driver development as well

\item

    We cannot meet our objectives with a \textbf{custom extension} because the
    financial burden on our team to maintain a total hard fork of not just
    toolchains, but also entire GNU/Linux Distros, is highly undesirable, and
    completely impractical (we know for certain that Redhat would strongly
    object to any efforts to hard-fork Fedora)

\item

    We could invent our own custom GPU instruction set (or use and extend an
    existing one, to save a man-decade on toolchain development) however even
    to switch over to that \textbf{Dual ISA} GPU instruction set in the next clock
    cycle still requires a PCR modeswitch bit in order to avoid needing a full
    Inter-Processor Bus Architecture like on \textbf{traditional} GPUs.

\item

    If extending any instruction set, rather than have a Dual ISA (which needs
    the PCR modeswitch bit to access it) we would rather extend POWER.

\item

    We cannot \textbf{go ahead anyway} because to do so would be highly irresponsible
    and cause massive disruption to the POWER community.

\end{itemize}

With all impractical options eliminated the only remaining responsible option
is to extend the POWER ISA in an atomically-managed (IANA-style) formal
fashion, whilst (critically and absolutely essentially) always providing a PCR
compatibility mode that is fully POWER compliant, including all illegal
instruction traps.


